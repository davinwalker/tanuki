# Tanuki!

Quick support resources access. Quickly copy file refences into the clipboard, view templates and changelogs.

![example](/docs/example.gif)

## Installation

```
git clone git@gitlab.com:davinwalker/tanuki.git
cd tanuki
bundle install
```

## Usage

```
bundle exec tanuki.rb
# > help

alias tanuki='/home/coder/tanuki/tanuki.rb'
tanuki
```

# TODO

- [ ] Installation/Bundle/Gem
- [x] Help
- [ ] CI Examples (pages/artifacts)
- [ ] Custom Hooks (pre-receive rb/py)
- [ ] [Bash completion](https://iridakos.com/tutorials/2018/03/01/bash-programmable-completion-tutorial.html)




### Other

```
export PROMPT='%{$fg_bold[green]%}tanuki ~%{$reset_color%} '
```
