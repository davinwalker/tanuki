# frozen_string_literal: true

# Blank Inheriter
module Auto
  def choices
    self.class.auto.map { |x| x.split(' ', 2).last }.uniq
  end

  def prompt
    prompter = TTY::Prompt.new(active_color: :bright_blue)
    @data = prompter.select('Wat you wana do?', choices)
  end
end

# Auto Complete Helper
module AutoComplete
  def self.check(command)
    all.select do |helper|
      helper.commands.include? command
    end
  end

  def self.get_line(line)
    command, data = line.split(' ', 2)
    [
      command.tr('.', '_'),
      data
    ]
  end

  def self.run(line)
    # binding.pry
    command, data = get_line(line)
    options = check(command)
    if options.count == 1
      options.first.new(data).send(command)
      # options.first.send(command.to_sym)
    elsif options.count > 1
      puts options
    else
      puts 'Available Commands'
      puts all_commands
    end
  end

  def self.all_commands
    # Remove common commands
    all.map(&:commands).flatten - %w[choices prompt]
  end

  def self.all
    ObjectSpace.each_object(Class).select do |c|
      c.included_modules.include? Auto
    end
  end

  def self.build
    all.each_with_object([]) do |x, obj|
      obj.concat x.auto
      obj
    end
  end

  def self.list
    @list ||= build.sort
  end

  def self.search(term)
    list.grep(/^#{Regexp.escape(term)}/)
  end
end
