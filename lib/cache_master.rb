# Helper to cache and view files
module CacheMaster
  def self.view(uri, markdown = false)
    spin

    file = 'raw/' + uri.gsub(/[^0-9A-Za-z\.]/, '_')
    write(uri, file, markdown) if mtime?(file) > 2

    stop
    File.read(file)
  end

  def self.write(uri, file, markdown)
    resp = HTTParty.get(uri)
    data = resp.body
    data = TTY::Markdown.parse(data) if markdown
    File.write(file, data)
  end

  # Hours modified time
  def self.mtime?(file)
    # Assume outdated
    return 5 unless File.exist?(file)

    # In Hours
    (Time.now - File.mtime(file)) / 60 / 60
  end

  def self.spin
    @spinner = TTY::Spinner.new
    @spinner.auto_spin
  end

  def self.stop
    @spinner.stop('Done')
  end
end
