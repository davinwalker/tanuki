# frozen_string_literal: true

# Common Commands
class Command
  def self.commands
    (instance_methods - Object.methods).map(&:to_s)
  end

  def initialize(data)
    @data = data.chomp(' ') if data
  end
end
