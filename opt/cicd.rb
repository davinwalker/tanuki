# frozen_string_literal: true

# Helper
class Ci < Command
  include Auto

  def self.help
    'Sample .gitlab-ci.yml templates'
  end

  def self.auto
    [
      'ci runner config',
      'ci artifacts',
      'ci simple',
      'ci debug tracing'
    ]
  end

  def ci
    prompt unless @data
    case @data
    when 'simple'         then simple
    when 'runner config'  then runner_config
    when 'artifacts'      then artifacts
    when 'debug tracing'  then debug_tracing
    end
  end

  private

  def debug_tracing
    text = 'https://docs.gitlab.com/ee/ci/variables/#debug-tracing'
    flavor = <<~TEXT
      ------
      # Add to Job CI
      variables:
        CI_DEBUG_TRACE: "true"
    TEXT
    puts Pastel.new.decorate(text, :green)
    puts Pastel.new.decorate(flavor, :blue)
    Clipboard.copy text
  end

  def simple
    text = File.read('templates/simple.yaml')
    puts Pastel.new.decorate(text, :green)
    Clipboard.copy text
  end

  def artifacts
    artifacts_text
    text = File.read('templates/artifacts.yaml')
    puts Pastel.new.decorate(text, :green)
    Clipboard.copy text
  end

  def runner_config
    text = '/etc/gitlab-runner/config.toml'
    puts Pastel.new.decorate(text, :green)
    Clipboard.copy text
  end

  def artifacts_text
    pastel = Pastel.new
    text = 'Project must be public to browse artifacts', :green, :bold
    puts pastel.decorate(*text) + pastel.decorate(' - *copied*', :bright_black)
  end
end
