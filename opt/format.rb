# frozen_string_literal: true

# Helper
class Format < Command
  include Auto

  def self.help
    'Pull in bad formatted text blocks'
  end

  def self.auto
    [
      'format yaml',
      'format toml',
      'format gitlabrb'
    ]
  end

  def format
    prompt unless @data
    case @data
    when 'yaml'        then yaml
    when 'toml'        then toml
    when 'gitlabrb'    then gitlabrb
    end
  end

  private

  def gitlabrb
    puts Pastel.new.decorate('Enter Text finish with ".."', :green)
    $/ = '..'
    user_input = STDIN.gets

    out = user_input.split("\n").reject do |line|
      line[0] == '#'
    end.reject(&:empty?)[0..-2].join("\n")

    puts out
    Clipboard.copy out
  end

  def yaml
    puts Pastel.new.decorate('Enter Text finish with \n\n\n', :green)
    $/ = "\n\n\n"
    user_input = STDIN.gets

    out = YAML.safe_load(user_input).to_yaml
    puts out
    Clipboard.copy out
  end

  def toml
    puts Pastel.new.decorate('Enter Text finish with \n\n\n', :green)
    $/ = "\n\n\n"
    user_input = STDIN.gets

    hash = TOML::Parser.new(user_input).parsed
    out = TOML::Generator.new(hash).body
    puts out
    Clipboard.copy out
  end
end
