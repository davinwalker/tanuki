# frozen_string_literal: true

# Helper
class Git < Command
  include Auto

  def self.help
    'Git Related data'
  end

  def self.auto
    [
      'git data-dir'
    ]
  end

  def git
    prompt unless @data
    case @data
    when 'data-dir' then data_dir
    end
  end

  private

  def data_dir
    text = '/var/opt/gitlab/git-data/repositories'
    puts Pastel.new.decorate(text, :green)
    Clipboard.copy text
  end
end
