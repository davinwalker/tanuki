# Helper
class Help < Command
  include Auto

  def self.help
    'really, you helped help?'
  end

  def self.auto
    %w[
      help quit exit
    ]
  end

  def help
    AutoComplete.all.sort_by(&:to_s).each do |helper|
      puts "#{title_text(helper.to_s)} - #{desc_text(helper.help)}\n"
      puts Pastel.new.decorate(helper_text(helper.auto), :bright_blue)
      puts
    end
  end

  private

  def helper_text(auto)
    auto.map { |x| '  ' + x.split(' ', 2).last }.uniq.join("\n")
  end

  def title_text(title)
    Pastel.new.decorate(title, :bright_yellow, :bold)
  end

  def desc_text(desc)
    Pastel.new.decorate(desc, :green, :italic, :dim)
  end

  def quit
    exit
  end
end
