# frozen_string_literal: true

# Helper
class Logs < Command
  include Auto

  def self.help
    'copy log locations into the clipboard'
  end

  def self.log_files
    [
      'sidekiq', 'gitlab-runner', 'registry', 'production_log',
      'unicorn', 'workhorse', 'gitlab-shell', 'gitaly', 'production_json_log'
    ]
  end

  def self.auto
    log_files.map do |file|
      [
        'log ' + file,
        'logs ' + file
      ]
    end.flatten.sort
  end

  def logs
    log
  end

  def log
    prompt unless @data

    case @data
    when 'workhorse' then workhorse
    when 'gitlab-shell' then gitlab_shell

    when 'sidekiq' then sidekiq
    when 'registry' then registry
    when 'unicorn' then unicorn
    when 'gitaly' then gitaly
    when 'production_json_log' then production_json
    when 'production_log' then production
    when 'gitlab-runner' then gitlab_runner
    end
  end

  private

  def gitlab_runner
    path = 'journalctl -u gitlab-runner'
    puts Pastel.new.decorate('The logs for service go to syslog or in case of systemd-based system', :bright_yellow, :bold)
    puts Pastel.new.decorate(path, :bright_blue, :bold)
    Clipboard.copy path
  end

  def unicorn
    path = '/var/log/gitlab/unicorn/unicorn_stderr.log'
    puts Pastel.new.decorate('/var/log/gitlab/unicorn/unicorn_stdout.log', :bright_yellow, :bold)
    puts Pastel.new.decorate(path, :bright_blue, :bold)
    Clipboard.copy path
  end

  def logger(path)
    Clipboard.copy path
    puts Pastel.new.decorate(path, :green)
  end

  def gitlab_shell
    logger '/var/log/gitlab/gitlab-shell/gitlab-shell.log'
  end

  def workhorse
    logger '/var/log/gitlab/gitlab-workhorse/current'
  end

  def production
    logger '/var/log/gitlab/gitlab-rails/production.log'
  end

  def gitaly
    logger '/var/log/gitlab/gitaly/current'
  end

  def production_json
    logger '/var/log/gitlab/gitlab-rails/production_json.log'
  end

  def sidekiq
    logger '/var/log/gitlab/sidekiq/current'
  end

  def registry
    logger '/var/log/gitlab/registry/current'
  end
end
