# frozen_string_literal: true

# Helper
class Registry < Command
  include Auto

  def self.help
    'registry files - logs,config'
  end

  def self.auto
    [
      'registry config',
      'registry logs',
      'registry log'
    ]
  end

  def registry
    prompt unless @data

    case @data
    when 'config' then config
    when 'log' then logs
    when 'logs' then logs
    end
  end

  private

  def logs
    path = '/var/log/gitlab/registry/current'
    puts Pastel.new.decorate(path, :bright_blue, :bold)
    Clipboard.copy path
  end

  def config
    path = '/var/opt/gitlab/registry/config.yml'
    puts Pastel.new.decorate(path, :bright_blue, :bold)
    Clipboard.copy path
  end
end
