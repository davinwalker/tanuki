# frozen_string_literal: true

# Helper
class Runner < Command
  include Auto

  def self.help
    'Runner specific configuration and examples'
  end

  def self.auto
    [
      'runner config',
      'runner logs'
    ]
  end

  def runner
    prompt unless @data

    case @data
    when 'config' then config
    when 'logs' then logs
    end
  end

  private

  def config
    path = '/etc/gitlab-runner/config.toml'
    puts Pastel.new.decorate(path, :bright_blue, :bold)
    Clipboard.copy path
  end

  def logs
    path = 'journalctl -u gitlab-runner'
    puts Pastel.new.decorate('The logs for service go to syslog or in case of systemd-based system', :bright_yellow, :bold)
    puts Pastel.new.decorate(path, :bright_blue, :bold)
    Clipboard.copy path
  end
end
