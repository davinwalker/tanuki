# frozen_string_literal: true

# Helper
class Read < Command
  include Auto

  def self.help
    'view/read gitlab resources'
  end

  def self.auto
    [
      'read gitlab.rb',
      'view gitlab.rb',
      'view changelog',
      'read changelog'
    ]
  end

  def view
    read
  end

  def read
    prompt unless @data

    case @data
    when 'gitlab.rb' then gitlab_rb
    when 'changelog' then changelog
    end
  end

  private

  def vim
    "vim --not-a-term +':set nonumber' +'noremap q :quit!' -"
  end

  def gitlab_rb_uri
    'https://gitlab.com/gitlab-org/omnibus-gitlab/raw/master/files/gitlab-config-template/gitlab.rb.template'
  end

  def gitlab_rb
    out = CacheMaster.view gitlab_rb_uri
    TTY::Pager.new(command: vim).page(out)
  end

  def changelog_uri
    'https://gitlab.com/gitlab-org/gitlab-ee/raw/master/CHANGELOG.md'
  end

  def changelog
    out = CacheMaster.view(changelog_uri, true)
    TTY::Pager.new.page(out)
  rescue Errno::EPIPE
    nil
  end
end
