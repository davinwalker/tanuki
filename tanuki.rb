#!/usr/bin/env ruby
# frozen_string_literal: true

require 'readline'
require 'clipboard'
require 'httparty'
require 'tty-pager'
require 'tty-prompt'
require 'tty-markdown'
require 'tty-spinner'
require 'pastel'
require 'toml'

Dir.chdir File.dirname(__FILE__)
Dir['lib/*.rb'].each { |file| require_relative file }
Dir['opt/*.rb'].each { |file| require_relative file }

Readline.completion_append_character = ' '
Readline.completion_case_fold = true
Readline.completer_word_break_characters = "\n"
Readline.completion_proc = proc do |term, _whole|
  AutoComplete.search term
end

def readline_with_hist_management
  line = Readline.readline('> ', true)
  return nil if line.nil?

  if line =~ /^\s*$/ || (Readline::HISTORY.to_a[-2] == line)
    Readline::HISTORY.pop
  end
  line
end

# exit 0

begin
  loop do
    line = readline_with_hist_management
    exit 0 if line == 'exit'
    next if line.empty?

    AutoComplete.run(line)
  end
rescue Interrupt
  puts 'Bai'
  exit
end
