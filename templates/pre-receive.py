#!/usr/bin/python

import sys
import re
import subprocess

line = sys.stdin.read()
(base, commit, ref) = line.strip().split()
branch_push = re.search('[a-z]', base)
print base
print commit
print ref

if '0000000000' in base or '0000000000' in commit:
    exit(0)

# base = 'c93645ec1d3f20d4696e8ce529190d1b5a278dcf'
# commit = 'd115dccddf4cf70a549b9656e6405b6307dd1e91'
print ' '.join(['git', 'rev-list','--oneline','--first-parent',base,commit])
commits = subprocess.Popen(['git', 'rev-list','--oneline','--first-parent',base,commit], stdout=subprocess.PIPE)
for line in commits.stdout.readlines():
    commit_list=str(line.strip()).split()
    commit_message=' '.join(commit for commit in commit_list[1:])
    jira_message=commit_message.split(':')[0].strip()
    print("********************************************************************")
    print("Checking JIRA =>"+jira_message +"<= .....")
    print("Checking JIRA =>"+commit_message +"<= .....")
    if re.search(':',commit_message):
        print("yaaay")
    else:
        print("Commit does not contain the JIRA ID with format: CFH-725:Updating the method.......Aborting Commit !!!!")
        print("********************************************************************")
        exit(1)

print("********************************************************************")
exit(0)
