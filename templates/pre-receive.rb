#!/usr/bin/env ruby
# frozen_string_literal:true

STDOUT.sync = true
$stdout.reopen('/tmp/pre-receive.txt', 'a')
$stderr.reopen('/tmp/pre-receive.txt', 'a')

puts Time.now

# chmod +x pre-receive

# Get and Split STDIN
# rev_old = 'cd26f8ce99939d657d3b7cfd2712e8b757c0a068'
# rev_new = '41cb3bf019ceac5057d21a44f64f24c179825376'
# ref = 'refs/heads/master'
rev_old, commit, ref = STDIN.read.split(' ')
puts "#{rev_old} - #{commit} - #{ref}"

# Check if creating new branch or deleting existing branch in remote repo
exit(0) if rev_old.include?('0000000000') || commit.include?('0000000000')

output = `git rev-list --oneline --first-parent #{rev_old} #{commit}`
puts output.inspect

puts '----------------'

message = output.split("\n").first
puts message.inspect

if message.include? 'fail'
  exit 1
else
  exit 0
end
